<?php 
    $num1 = $_POST['number1'];
    $opt = $_POST['operador'];
    $num2 = $_POST['number2'];
    // echo "El numero 1 es ${num1} el operador es ${opt} el numero dos es ${num2}";

    switch ($opt) {
        case 'sum':
            $result = $num1 + $num2;
            break;
        case 'res':
            $result = $num1 - $num2;
            break;
        case 'mult':
            $result = $num1 * $num2;
            break;
        case 'div':
            if ($num2 != 0) {
                $result = $num1 / $num2;
                break;
            } else {
                $result = 'No se puede dividir por 0';
                break;
            }
    }

    echo json_encode($result);

?>