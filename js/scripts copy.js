var a = document.getElementById("number1")
var b = document.getElementById("number2")
var opt = document.getElementById("opt")

var result = document.getElementById("result")

function calcular(a, b, opt) {
    var total
    switch (opt.value) {
        case 'sum':
            total = Number(a.value) + Number(b.value)
            break;
        case 'res':
            total = Number(a.value) - Number(b.value)
            break;
        case 'mult':
            total = Number(a.value) * Number(b.value)
            break;
        case 'div':
            if(Number(b.value) == 0){
                alert("No se puede dividir por 0")
                total = 0
            } else {
                total = Number(a.value) / Number(b.value)
            }
            break;
    }
    return total
}
document.querySelector("#calc").addEventListener("submit", function(e){
    var x = calcular(a, b, opt)
    result.innerHTML = `${x}`
    e.preventDefault()
})
