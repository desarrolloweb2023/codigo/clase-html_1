fetch('https://restcountries.com/v3.1/all')
.then(res => res.json())
.then(data => {
    console.log(data)
    $("#myTable").dataTable({
        "data": data.map(country => {
            return {
                "name": country.name.common,
                "capital": country.capital ? country.capital : 'Sin Datos',
                "population": country.population,
                "flag": country.flag
            }
        }),
        "columns": [
            {"data": "name"},
            {"data": "capital"},
            {"data": "population"},
            {"data": "flag"}
        ]
    })
    // $("#myTable").dataTable({
    //     "data": data,
    //     "columns": [
    //         {"data": "name.common"},
    //         {"data": "capital"},
    //         {"data": "population"},
    //         {"data": "flag"}
    //     ]
    // })
})


