var form = document.getElementById('calc');
var result = document.getElementById('result');

form.addEventListener('submit', function(e){
    e.preventDefault();
    var action = form.getAttribute('action');
    var methodForm = form.getAttribute('method');
    var dataForm = new FormData(form);

    fetch(action,{
        method: methodForm,
        body: dataForm
    })
    .then(res => res.json())
    .then( data => {
        console.log(data);
        result.innerHTML = `${data}`
    })

})


